# Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
#
# This file is part of AIM.
#
# AIM is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AIM is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

include $(top_srcdir)/env.am

ACLOCAL_AMFLAGS = -I ./m4

if BUILD_BOOTLOADER
BOOT = boot
endif

SUBDIRS = \
	include \
	lib \
	$(BOOT) \
	kern

QEMU_OPTS=-drive file=disk.img,format=raw,index=0

qemu:
	@qemu-system-i386 -enable-kvm -display sdl $(QEMU_OPTS)
gdb:
	qemu-system-i386 -display none -S -s $(QEMU_OPTS) > /dev/null 2>/dev/null &
	gdb kern/vmaim.elf
	echo -e "k\nq\n" | gdb > /dev/null 2>/dev/null &

disk:
	$(top_srcdir)/create-disk.sh

disk-gpt:
	$(top_srcdir)/create-disk.sh gpt

inst-boot: disk.img
	$(MAKE) -C boot
	dd if=boot/boot1.bin of=disk.img conv=notrunc
	dd if=boot/boot2.bin of=disk.img conv=notrunc seek=34

inst-kern: disk.img
	$(MAKE) -C kern
	dd if=kern/vmaim.elf of=disk.img conv=notrunc seek=4096

inst: inst-boot inst-kern
