
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <aim/boot.h>
#include <elf.h>

void bootstep2(void *loadpos) __attribute__ ((section(".entry")));
void _start() __attribute__ ((alias("bootstep2")));

__noreturn
void bootstep2(void *loadpos)
{
	uint32_t filesec;
	if (PART_TABLE[0].type == 0xEE) { // GPT Presents
		gpt_header header;
		readsect(&header, 1);
		char gpt_magic[] = "EFI PART";
		for (int i = 0; i < 4; i++) {
			if (header.magic[i] != gpt_magic[i]) while (1); // panic
		}
		// other validations omitted
		gpt_entry ent;
		readseg(&ent, 2, 128, 128);
		filesec = (uint32_t)ent.lba_start;
	} else {
		filesec = PART_TABLE[1].lba_start;
	}
	elf32hdr_t *elf = (elf32hdr_t *)(loadpos + STEP2_SIZE);
	readsects(elf, filesec, ELF_HEAD_SIZE / SECTSIZE);

    // validate ELF
	char elf_magic[] = "\177ELF";
	for (int i = 0; i < 4; i++) {
		if (elf->e_ident[i] != elf_magic[i]) while (1); // panic
	}

	elf32_phdr_t *const ph = (void *)elf + elf->e_phoff;
	elf32_phdr_t *const eph = ph + elf->e_phnum;

	// check for valid position
	for (elf32_phdr_t *p = ph; p < eph; p++) {
		if (p->p_type & PT_LOAD) {
			if ((void *)p->p_paddr < loadpos + STEP2_PRESERVE_SIZE &&
				(void *)p->p_paddr + p->p_memsz > loadpos) {
					// overlapped, calc a new position
					loadpos = (void *)p->p_paddr + p->p_memsz;
					loadpos = arch_find_place(loadpos, STEP2_PRESERVE_SIZE);

					readsects(loadpos, STEP2_LBA, STEP2_SIZE / SECTSIZE);
					setstack(loadpos + STEP2_PRESERVE_SIZE);
					((void (*)(void *))loadpos)(loadpos); // should not return
				}
		}
	}

	// load file
	for (elf32_phdr_t *p = ph; p < eph; p++) {
		if (p->p_type & PT_LOAD) {
			readseg((void *)p->p_paddr, filesec, p->p_offset, p->p_filesz);
			mmemset((uint8_t *)p->p_paddr + p->p_filesz, 0, p->p_memsz - p->p_filesz);
		}
	}

    // go to entry
    ((void (*)(void))elf->e_entry)();
	while (1);
}
