#include <aim/boot.h>
void *arch_find_place(void *_start, uint32_t len) {
    uint32_t start = (uint32_t)_start;
    if (start < 0x7E00) start = 0x7E00;
    if (start <= 0x80000 && start + len > 0x80000) start = 0x100000;
    if (start <= 0xF00000 && start + len > 0xF00000) start = 0x1000000;
    return (void *)start;
}
