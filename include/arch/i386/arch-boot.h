/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ARCH_BOOT_H
#define _ARCH_BOOT_H

#ifndef __ASSEMBLER__

static inline uint8_t inb(uint16_t port)
{
	uint8_t result;
	asm volatile ("inb %w1, %b0" : "=a"(result) : "Nd"(port));
	return result;
}

static inline void outb(uint8_t value, uint16_t port)
{
	asm volatile ("outb %b0, %w1" :: "a"(value), "Nd"(port));
}

static inline void waitdisk()
{
	while ((inb(0x1F7) & 0xC0) != 0x40);
}

static inline void insl(uint16_t port, void *addr, uint32_t count) {
	asm volatile ("cld ; rep ; insl":"=D" (addr), "=c" (count) :"d" (port), "0" (addr), "1" (count) : "memory", "cc");
}

static inline void readsect(void *dst, uint32_t lba) {
    waitdisk();
    outb(1, 0x1F2); // count = 1
    outb(lba, 0x1F3);
    outb(lba >> 8, 0x1F4);
    outb(lba >> 16, 0x1F5);
	outb(0xE0 | ((lba >> 24) & 0x0F), 0x1F6);
    outb(0x20, 0x1F7);

    waitdisk();
    insl(0x1F0, dst, SECTSIZE / 4);
}

#define setstack(stack_pointer) \
    asm volatile ("mov %0, %%esp" :: "g" (stack_pointer))

#endif /* !__ASSEMBLER__ */

#endif /* !_ARCH_BOOT_H */
