/* Copyright (C) 2016 David Gao <davidgao1001@gmail.com>
 *
 * This file is part of AIM.
 *
 * AIM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AIM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _AIM_BOOT_H
#define _AIM_BOOT_H

#define SECTSIZE 512
#define START ((void *)0x7C00)
#define PART_TABLE ((part_entry *)(0x7C00 + 0x1BE))
#define ELF_HEAD_SIZE 4096
#define STEP2_LBA 34
#define STEP2_SECTS 10
#define STEP2_SIZE (STEP2_SECTS * SECTSIZE)
#define STEP2_STACK_SIZE 0x4000
#define STEP2_PRESERVE_SIZE (STEP2_SIZE + ELF_HEAD_SIZE + STEP2_STACK_SIZE)

#ifndef __ASSEMBLER__
#include <sys/types.h>
static void readsect(void *dst, uint32_t lba);

#pragma pack(push, 1)
typedef struct part_entry {
	uint8_t status;
	uint32_t chs_start : 24;
	uint8_t type;
	uint32_t chs_end : 24;
	uint32_t lba_start;
	uint32_t nsectors;
} part_entry;

typedef struct gpt_header {
    uint8_t magic[8];
    uint8_t version[4];
    uint32_t header_length;
    uint32_t header_crc32;
    uint8_t __reserved;
    uint64_t current_lba;
    uint64_t backup_lba;
    uint64_t first_avail_lba;
    uint64_t last_avail_lba;
    uint8_t guid[16];
    uint64_t entries_start_lba;
    uint32_t num_entries;
    uint32_t entry_length;
    uint32_t entries_crc32;
} gpt_header;

typedef struct gpt_entry {
    uint8_t type[16];
    uint8_t part_guid[16];
    uint64_t lba_start;
    uint64_t lba_end;
    uint8_t flags[8];
    uint16_t part_name[72];
} gpt_entry;

#pragma pack(pop)

static inline void readsects(void *dst, uint32_t lba, uint32_t nsectors) {
	while (nsectors--) {
		readsect(dst, lba);
		dst += SECTSIZE;
		lba++;
	}
}

static inline void mmemcpy(void *_dst, void *_src, uint32_t len) {
    uint8_t *dst = _dst, *src = _src;
    while (len-- > 0) *dst++ = *src++;
}

static inline void mmemset(void *_dst, uint8_t value, uint32_t len) {
	uint8_t *dst = _dst;
	while (len-- > 0) *dst++ = value;
}

static inline void readseg(void *dst, uint32_t lba, uint32_t off, uint32_t len) {
    lba += off / SECTSIZE;
    off %= SECTSIZE;
    uint8_t buf[SECTSIZE];
	while (len > 0) {
		readsect(buf, lba);
        uint32_t clen = len;
        if (SECTSIZE < clen) clen = SECTSIZE;
        mmemcpy(dst, buf + off, clen);
        lba++;
        off += clen - SECTSIZE;
        len -= clen;
        dst += clen;
	}
}

void *arch_find_place(void *pos, uint32_t len);

#endif /* !__ASSEMBLER__ */

#include <arch-boot.h>

#endif /* !_AIM_BOOT_H */
