#!/usr/bin/env bash

dd if=/dev/zero of=disk.img count=8192
if [ "$1" == "gpt" ]; then
    sed -r 's/^\s*(\S.*)\s*/\1/' <<EOF | fdisk disk.img
        g
        n
        1
        2048
        4095
        n
        2
        4096
        8158
        t
        1
        9bcb6514-53d1-46e7-afaf-34fe99911036
        t
        2
        fe284f9e-4c67-4de3-92a6-7440212b48d9
        w
EOF
else
    sed -r 's/^\s*(\S.*)\s*/\1/' <<EOF | fdisk disk.img
        o
        n
        p
        1
        2048
        4095
        n
        p
        2
        4096
        8191
        t
        1
        2d
        t
        2
        2e
        w
EOF
fi
